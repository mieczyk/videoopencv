# OpenCV Video Player

This is a simple video player written in Java, using OpenCV 2.4.11.

## Requirements for Windows:

* JDK 1.8 and Java 8 (lambda expressions).
* OpenCV 2.4.11.

## Run on Windows:

Assuming that OpenCV installation folder is `<OPEN_CV> = C:\Program Files\OpenCV` and architecture is x64.

1. Add OpenCV JAR to project (`<OPEN_CV>\build\java\opencv-2411.jar`).
2. Add DLL library to your `java.library.path`. You can run application with argument:

    ```
    -Djava.library.path="<OPEN_CV>\build\java\x64"
    ```

3. Add `<OPEN_CV>\build\x64\vc12\bin` to PATH environment variable.