import org.opencv.core.Mat;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class CvUtils {
    public static Image cvMatToImage(Mat img) {
        int type = BufferedImage.TYPE_BYTE_GRAY;

        if(img.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }

        int bufferSize = img.channels() * img.cols() * img.rows();
        byte[] buffer = new byte[bufferSize];

        // Get all pixels
        img.get(0, 0, buffer);

        BufferedImage resultImage = new BufferedImage(img.cols(), img.rows(), type);

        final byte[] targetPixels = ((DataBufferByte)resultImage.getRaster().getDataBuffer()).getData();

        System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);

        return resultImage;
    }
}
