import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import java.awt.*;

public class Video {
    private VideoCapture _videoCapture;

    public Video(String source) throws CannotOpenVideoException {
        _videoCapture = new VideoCapture(source);

        if(!_videoCapture.isOpened()) {
            throw new CannotOpenVideoException(source);
        }
    }

    public Video(int webcam) throws CannotOpenVideoException {
        _videoCapture = new VideoCapture(webcam);

        if(!_videoCapture.isOpened()) {
            throw new CannotOpenVideoException(webcam);
        }
    }

    public Image getFrame(int width, int height) {
        Mat frame = new Mat();
        _videoCapture.read(frame);

        Imgproc.resize(frame, frame, new Size(width, height));

        Image resultImage = null;

        if(frame.dataAddr() > 0) {
            resultImage = CvUtils.cvMatToImage(frame);
        }

        frame.release();

        return resultImage;
    }
}