import javax.swing.*;
import java.awt.*;

public class VideoPlayerWorker extends SwingWorker<Void, Image> {
    private final JLabel _videoPlayerLabel;
    private final Video _video;

    private volatile boolean _isPaused;

    public VideoPlayerWorker(Video video, JLabel videoPlayerLabel) {
        _videoPlayerLabel = videoPlayerLabel;
        _video = video;

        _isPaused = false;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Image frame = _video.getFrame(_videoPlayerLabel.getWidth(), _videoPlayerLabel.getHeight());

        while(!isCancelled()) {
            if(!_isPaused && frame != null) {
                publish(frame);
                frame = _video.getFrame(_videoPlayerLabel.getWidth(), _videoPlayerLabel.getHeight());
            }
            Thread.sleep(40);
        }
        return null;
    }

    @Override
    protected void process(java.util.List<Image> frames) {
        for(Image frame : frames) {
            _videoPlayerLabel.setIcon(new ImageIcon(frame));
        }
    }

    public void pause() {
        _isPaused = true;
    }

    public void resume() {
        _isPaused = false;
    }
}
