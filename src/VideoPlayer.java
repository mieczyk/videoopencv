import org.opencv.core.Core;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;

public class VideoPlayer extends JFrame {
    private JPanel _videoPlayerPanel;
    private JButton _playButton;
    private JButton _stopButton;
    private JLabel _videoPlayerLabel;
    private File _lastDir;
    private VideoPlayerWorker _videoPlayerWorker;

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public VideoPlayer() {
        super("OpenCV Video Player");

        setContentPane(_videoPlayerPanel);
        _playButton.addActionListener(e -> play());
        _stopButton.addActionListener(e -> stop());

        createMenu();

        pack();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);

        _lastDir = new File("D:\\Movies");

        _videoPlayerWorker = null;
    }

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(() -> new VideoPlayer());
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        JMenuItem openMenuItem = new JMenuItem("Open", KeyEvent.VK_O);
        openMenuItem.addActionListener(e -> open());
        fileMenu.add(openMenuItem);

        JMenuItem exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_X);
        exitMenuItem.addActionListener(e -> exit());
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);
    }

    private void open() {
        final FileFilter videoFilesFilter = new FileNameExtensionFilter(
                "Video files (*.avi, *.mp4, *.mov, *.3gp, *.rmvb, *.rm, *.wmv, *.mkv ,*.flv)",
                "avi", "mp4", "mov", "3gp", "rmvb", "rm", "wmv", "mkv", "flv"
        );

        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(videoFilesFilter);
        fileChooser.setFileFilter(videoFilesFilter);

        fileChooser.setCurrentDirectory(_lastDir);

        if(fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            _lastDir = fileChooser.getCurrentDirectory();

            try {
                Video video = new Video(fileChooser.getSelectedFile().getPath());

                if(_videoPlayerWorker != null) {
                    _videoPlayerWorker.cancel(true);
                }

                _videoPlayerWorker = new VideoPlayerWorker(video, _videoPlayerLabel);

                _playButton.setEnabled(true);
                _stopButton.setEnabled(false);
            } catch (CannotOpenVideoException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void play() {
        _playButton.setEnabled(false);
        _stopButton.setEnabled(true);

        SwingWorker.StateValue state = _videoPlayerWorker.getState();

        if(state == SwingWorker.StateValue.PENDING) {
            _videoPlayerWorker.execute();
        } else {
            _videoPlayerWorker.resume();
        }
    }

    private void stop() {
        _playButton.setEnabled(true);
        _stopButton.setEnabled(false);

        _videoPlayerWorker.pause();
    }

    private void exit() {
        this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }
}
