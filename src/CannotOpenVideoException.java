public class CannotOpenVideoException extends Exception {
    public CannotOpenVideoException(String source) {
        super("Cannot open video file: " + source);
    }

    public CannotOpenVideoException(int webcam) {
        super("Cannot open web cam stream: " + webcam);
    }
}
